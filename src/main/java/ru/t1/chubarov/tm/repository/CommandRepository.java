package ru.t1.chubarov.tm.repository;

import ru.t1.chubarov.tm.api.repository.ICommandRepository;
import ru.t1.chubarov.tm.constant.ArgumentConst;
import ru.t1.chubarov.tm.constant.CommandConst;
import ru.t1.chubarov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    public static final Command ABOUT = new Command(CommandConst.ABOUT, ArgumentConst.ABOUT, "Show about program.");

    public static final Command VERSION = new Command(CommandConst.VERSION, ArgumentConst.VERSION, "Show program version.");

    public static final Command HELP = new Command(CommandConst.HELP, ArgumentConst.HELP,"Show list arguments.");

    public static final Command INFO = new Command(CommandConst.INFO, ArgumentConst.INFO, "Show system info.");

    public static final Command COMMANDS = new Command(CommandConst.COMMANDS, ArgumentConst.COMMANDS, "Show command list.");

    public static final Command ARGUMENTS = new Command(CommandConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Show argument list.");

    public static final Command EXIT = new Command(CommandConst.EXIT, null, "Close application");

    public static final Command PROJECT_LIST = new Command(CommandConst.PROJECT_LIST, null, "Show list project.");

    public static final Command PROJECT_CREATE = new Command(CommandConst.PROJECT_CREATE, null, "Create new project.");

    public static final Command PROJECT_CLEAR = new Command(CommandConst.PROJECT_CLEAR, null, "Remove all project.");

    public static final Command PROJECT_REMOVE_BY_ID = new Command(CommandConst.PROJECT_REMOVE_BY_ID, null, "Remove project by Id.");

    public static final Command PROJECT_REMOVE_BY_INDEX = new Command(CommandConst.PROJECT_REMOVE_BY_INDEX, null, "Remove project by index.");

    public static final Command PROJECT_SHOW_BY_ID = new Command(CommandConst.PROJECT_SHOW_BY_ID, null, "Display project by Id.");

    public static final Command PROJECT_SHOW_BY_INDEX = new Command(CommandConst.PROJECT_SHOW_BY_INDEX, null, "Display project by index.");

    public static final Command PROJECT_UPDATE_BY_ID = new Command(CommandConst.PROJECT_UPDATE_BY_ID, null, "Update project by Id.");

    public static final Command PROJECT_UPDATE_BY_INDEX = new Command(CommandConst.PROJECT_UPDATE_BY_INDEX, null, "Update project by index.");

    public static final Command PROJECT_CHANGE_STATUS_BY_ID = new Command(CommandConst.PROJECT_CHANGE_STATUS_BY_ID, null, "Change project status by id.");

    public static final Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(CommandConst.PROJECT_CHANGE_STATUS_BY_INDEX, null, "Change project status by index.");

    public static final Command PROJECT_START_BY_ID = new Command(CommandConst.PROJECT_START_BY_ID, null, "Update project status to [In progress] by id.");

    public static final Command PROJECT_START_BY_INDEX = new Command(CommandConst.PROJECT_START_BY_INDEX, null, "Update project status to [In progress] by index.");

    public static final Command PROJECT_COMPLETE_BY_ID = new Command(CommandConst.PROJECT_COMPLETE_BY_ID, null, "Update project status to [Completed] by id.");

    public static final Command PROJECT_COMPLETE_BY_INDEX = new Command(CommandConst.PROJECT_COMPLETE_BY_INDEX, null, "Update project status to [Completed] by index.");

    public static final Command TASK_LIST = new Command(CommandConst.TASK_LIST, null, "Show list task.");

    public static final Command TASK_CREATE = new Command(CommandConst.TASK_CREATE, null, "Create new task.");

    public static final Command TASK_CLEAR = new Command(CommandConst.TASK_CLEAR, null, "Remove all task.");

    public static final Command TASK_REMOVE_BY_ID = new Command(CommandConst.TASK_REMOVE_BY_ID, null, "Remove task by Id.");

    public static final Command TASK_REMOVE_BY_INDEX = new Command(CommandConst.TASK_REMOVE_BY_INDEX, null, "Remove task by index.");

    public static final Command TASK_SHOW_BY_ID = new Command(CommandConst.TASK_SHOW_BY_ID, null, "Display task by Id.");

    public static final Command TASK_SHOW_BY_INDEX = new Command(CommandConst.TASK_SHOW_BY_INDEX, null, "Display task by index.");

    public static final Command TASK_UPDATE_BY_ID = new Command(CommandConst.TASK_UPDATE_BY_ID, null, "Update task by Id.");

    public static final Command TASK_UPDATE_BY_INDEX = new Command(CommandConst.TASK_UPDATE_BY_INDEX, null, "Update task by index.");

    public static final Command TASK_CHANGE_STATUS_BY_ID = new Command(CommandConst.TASK_CHANGE_STATUS_BY_ID, null, "Change task status by id.");

    public static final Command TASK_CHANGE_STATUS_BY_INDEX = new Command(CommandConst.TASK_CHANGE_STATUS_BY_INDEX, null, "Change task status by index.");

    public static final Command TASK_START_BY_ID = new Command(CommandConst.TASK_START_BY_ID, null, "Update task status to [In progress] by id.");

    public static final Command TASK_START_BY_INDEX = new Command(CommandConst.TASK_START_BY_INDEX, null, "Update task status to [In progress] by index.");

    public static final Command TASK_COMPLETE_BY_ID = new Command(CommandConst.TASK_COMPLETE_BY_ID, null, "Update task status to [Completed] by id.");

    public static final Command TASK_COMPLETE_BY_INDEX = new Command(CommandConst.TASK_COMPLETE_BY_INDEX, null, "Update task status to [Completed] by index.");
    public static final Command TASK_BIND_TO_PROJECT = new Command(CommandConst.TASK_BIND_TO_PROJECT, null, "Bind task to project bi id.");
    public static final Command TASK_UNBIND_FROM_PROJECT = new Command(CommandConst.TASK_UNBIND_FROM_PROJECT, null, "Unbind task from project bi id.");
    public static final Command TASK_SHOW_BY_PROJECT_ID = new Command(CommandConst.TASK_SHOW_BY_PROJECT_ID, null, "Display tasks related to the project.");

    public static final Command[] TERMINAL_COMMANDS = new Command[]{
            ABOUT, VERSION, HELP, INFO, COMMANDS, ARGUMENTS,
            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX, PROJECT_SHOW_BY_ID, PROJECT_SHOW_BY_INDEX, PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX,
            PROJECT_CHANGE_STATUS_BY_ID, PROJECT_CHANGE_STATUS_BY_INDEX, PROJECT_START_BY_ID, PROJECT_START_BY_INDEX, PROJECT_COMPLETE_BY_ID, PROJECT_COMPLETE_BY_INDEX,
            TASK_LIST, TASK_CREATE, TASK_CLEAR,
            TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX, TASK_SHOW_BY_ID, TASK_SHOW_BY_INDEX, TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            TASK_CHANGE_STATUS_BY_ID, TASK_CHANGE_STATUS_BY_INDEX, TASK_START_BY_ID, TASK_START_BY_INDEX, TASK_COMPLETE_BY_ID, TASK_COMPLETE_BY_INDEX,
            TASK_BIND_TO_PROJECT, TASK_UNBIND_FROM_PROJECT, TASK_SHOW_BY_PROJECT_ID,
            EXIT};

    public  Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }
}
