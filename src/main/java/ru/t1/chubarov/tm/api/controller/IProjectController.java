package ru.t1.chubarov.tm.api.controller;

public interface IProjectController {
    void createProject();

    void showProjects();

    void clearProjects();

    void showProjectByIndex();

    void showProjectById();

    void updateProjectByIndex();

    void updateProjectById();

    void removeProjectByIndex();

    void removeProjectById();

    void changeProjectStatusByIndex();

    void changeProjectStatusById();

    void startProjectByIndex();

    void startProjectById();

    void completeProjectByIndex();

    void completeProjectById();
}
