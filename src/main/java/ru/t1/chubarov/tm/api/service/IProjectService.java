package ru.t1.chubarov.tm.api.service;

import ru.t1.chubarov.tm.api.repository.IProjectRepository;
import ru.t1.chubarov.tm.enumerated.Sort;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IProjectRepository {

    Project create (String name, String description);

    List<Project> findAll(Sort sort);

    List<Project> findAll(Comparator comparator);

    void remove(Project project);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project changeProjectStatusById(String id, Status status);

}
